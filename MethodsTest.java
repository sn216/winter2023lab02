public class MethodsTest
{
    public static void main (String args[])
    {
       SecondClass sc = new SecondClass();
       int x = sc.addTwo(50);
       System.out.println(x);
    }

    public static void methodNoInputNoReturn()
    {
        System.out.println("I'm in a method that takes no input and returns nothing");
        int x = 20;
        System.out.println(x);
    }

    public static void methodOneInputNoReturn(int yes)
    {
        yes = yes -5;
        System.out.println(yes);
    }

    public static void methodTwoInputNoReturn(int yes, double no)
    {
        System.out.println(yes + " " + no);
    }

    public static int methodNoInputReturnInt()
    {
        int a = 5;
        return a;
    }

    public static double sumSquareRoot(int b,int c)
    {
       double d = b + c;
       d = Math.sqrt(d);

       return d;
    }
}